package main

import (
	"fmt"
	"unicode"
)

var jumlah int = 0
var pembilang, penyebut = 0, 0
var kali, iterasi, SumDeretBilBulat int = 1, 1, 0
var Temp string = ""

func main() {

	var pilih, batas, submenu int
	var kata string
TopMenu:
	fmt.Println("=============================")
	fmt.Println("      Program Rekursif       ")
	fmt.Println("=============================")
	fmt.Println("        ===PILIHAN===        ")
	fmt.Println("[1]. Deret Punjumlahan angka ")
	fmt.Println("[2]. Deret Penjumlahan pecahan")
	fmt.Println("[3]. Menghitung huruf non KAPITAL")
	fmt.Println("[4]. Keluar")
	fmt.Println("=============================")
	fmt.Print("Masukkan pilihan: ")
	fmt.Scanf("%d\n", &pilih)
SelMenu:
	switch pilih {
	case 1:
		fmt.Println("=============================")
		fmt.Print("Masukan Batas deret : ")
		fmt.Scanf("%d\n", &batas)

		DeretBilBulat(batas)
	case 2:
		fmt.Println("=============================")
		fmt.Print("Masukan Batas deret : ")
		fmt.Scanf("%d\n", &batas)
		deretD(batas)
	case 3:
		fmt.Println("=============================")
		fmt.Print("Masukan sebuah Kata : ")
		fmt.Scanf("%s\n", &kata)
		jumlah = 0
		kapital(len(kata)-1, kata)
		fmt.Printf("jumlah huruf kecil: %d", jumlah)
	case 4:
		fmt.Println("=============================")
		fmt.Println("         Terima Kasih        ")
		fmt.Println("=============================")
	default: 
		fmt.Println("=============================")
		fmt.Print("        Keyword Salah")
	}

	if pilih != 4 {
		fmt.Println()
		fmt.Println("=============================")
		fmt.Println("9. Kembali ke menu")
		fmt.Println("0. Keluar")
	InputSub:
		fmt.Print("Pilihan anda : ")
		fmt.Scanf("%d\n", &submenu)
		if submenu == 9 {
			goto TopMenu
		} else if submenu == 0 {
			pilih = 4
			goto SelMenu
		} else {
			fmt.Println("=============================")
			fmt.Println("        Keyword Salah")
			fmt.Println("=============================")
			goto InputSub
		}
	}
}

func DeretBilBulat(jml int) {
	SumDeretBilBulat += iterasi * kali
	if jml <= 1 {
		Temp += fmt.Sprint(iterasi * kali)
		fmt.Print(SumDeretBilBulat, " = ", Temp)
		kali, iterasi, SumDeretBilBulat = 1, 1, 0
		Temp = ""
	} else {
		Temp += fmt.Sprint(iterasi*kali, " + ")
		iterasi++
		kali *= 10
		DeretBilBulat(jml - 1)
	}
}

func deretD(i int) {
	pembilang += iterasi
	penyebut := iterasi * 2

	if i <= 1 {
		Temp += fmt.Sprintf("%d/%dx", iterasi, iterasi*2)
		fmt.Printf("%d/%dx = %s", pembilang, penyebut, Temp)
		pembilang = 0
		penyebut = 0
		Temp = ""
		iterasi = 1
	} else {
		Temp += fmt.Sprintf("%d/%dx + ", iterasi, iterasi*2)
		iterasi++
		deretD(i - 1)
	}
}

func kapital(i int, x string) {
	if i >= 0 {
		 y := rune(x[i])
		 if unicode.IsLower(y) {
			 jumlah = jumlah + 1
		 }
	 
		 kapital(i-1, x)
	}
}
