package main

import (
	"fmt"
	"sort"
)

type ListItems struct {
	ID    int
	Nama  string
	Jam   int
	Biaya int
}

var WarnetData []ListItems

func dummy() {
	WarnetData = append(WarnetData, ListItems{1, "Risky", 2, 120000})
	WarnetData = append(WarnetData, ListItems{2, "Ardi", 1, 60000})
}

func main() {
	dummy()
TopMenu:
	var menu, submenu int
	fmt.Println("=================================")
	fmt.Println("         APLIKASI WARNET")
	fmt.Println("=================================")
	fmt.Println("Pilih menu dibawah ini")
	fmt.Println("1. Input Data")
	fmt.Println("2. Hapus Data")
	fmt.Println("3. Tampil Data")
	fmt.Println("4. Rata - Rata Jam Penggunaan")
	fmt.Println("5. Top 3 jam terbawah")
	fmt.Println("6. Pengguna dibawah rata - rata")
	fmt.Println("0. Keluar")
	fmt.Println("=================================")
	fmt.Print("Menu pilihan saya : ")
	fmt.Scanf("%d\n", &menu)

SelMenu:
	if menu == 0 {
		fmt.Println("=================================")
		fmt.Println("      ** Terimakasih **")
		fmt.Println("=================================")
	} else if menu == 1 {
		create()
	} else if menu == 2 {
		delete()
	} else if menu == 3 {
		show()
	} else if menu == 4 {
		fmt.Println(Avg(), "Jam")
	} else if menu == 5 {
		top()
	} else if menu == 6 {
		average()

	} else {
		fmt.Println("=================================")
		fmt.Println("        Keyword Salah")
		fmt.Println("=================================")
	}

	if menu != 0 {
		fmt.Println("9. Kembali ke menu")
		fmt.Println("0. Keluar")
	InputSub:
		fmt.Print("Pilihan anda : ")
		fmt.Scanf("%d\n", &submenu)
		if submenu == 9 {
			goto TopMenu
		} else if submenu == 0 {
			menu = 0
			goto SelMenu
		} else {
			fmt.Println("=================================")
			fmt.Println("        Keyword Salah")
			fmt.Println("=================================")
			goto InputSub
		}
	}
}

func sorting() {
	sort.SliceStable(WarnetData, func(i int, j int) bool {
		return WarnetData[i].ID < WarnetData[j].ID
	})
}

func exists(ID int) bool{
	for _, Value := range WarnetData {
		if Value.ID == ID {
			return true
		}
	}

	return false
}

func create() {
	Data := ListItems{}
	var Harga int = 1000
	fmt.Println("=================================")
	fmt.Println("          Input Data")
	fmt.Println("=================================")
	fmt.Print("Masukan ID [number]         : ")
	fmt.Scanf("%d\n", &Data.ID)
	fmt.Print("Masukan Nama [String]       : ")
	fmt.Scanf("%s\n", &Data.Nama)
	fmt.Print("Masukan Jumlah Jam [number] : ")
	fmt.Scanf("%d\n", &Data.Jam)
	fmt.Println("Harga warnet per menit      :", Harga)
	Data.Biaya = (Data.Jam * 60) * Harga
	fmt.Println("Biaya Warnet                :", Data.Biaya)
	fmt.Println("=================================")
	if exists(Data.ID) == false {
		WarnetData = append(WarnetData, Data)
		fmt.Println("       Input Berhasil")
	}else{
		fmt.Println(" Input Gagal ID Telah Digunakan")
	}
	fmt.Println("=================================")
	sorting()
}

func delete() {
	var id int
	var Temp []ListItems
	fmt.Println("=================================")
	fmt.Println("          Hapus Data")
	fmt.Println("=================================")
	fmt.Print("Masukan ID [number]   : ")
	fmt.Scanf("%d\n", &id)
	fmt.Println("=================================")
	if exists(id) == true {
		for _, Value := range WarnetData {
			if Value.ID != id {
				Temp = append(Temp, Value)
			}
		}
		WarnetData = Temp
		sorting()
		fmt.Println("         Hapus Berhasil          ")
	}else{
		fmt.Println(" Hapus Gagal ID Tidak Ditemukan")
	}
	fmt.Println("=================================")
}

func show() {
	fmt.Println("=================================")
	fmt.Println("          Tampil Data")
	fmt.Println("=================================")
	if len(WarnetData) > 0{
		fmt.Println("ID\tNama\tJam\tBiaya")
		for _, Value := range WarnetData {
			fmt.Printf("%d\t%s\t%d\t%d\n", Value.ID, Value.Nama, Value.Jam, Value.Biaya)
		}
	}else{
		fmt.Println("          Data Kosong")
	}
	fmt.Println("=================================")
	fmt.Println("Jumlah Data :", len(WarnetData))
	fmt.Println("=================================")
}

func Avg() float64 {
	fmt.Println("=================================")
	fmt.Println("    Rata - Rata Jam Penggunaan")
	fmt.Println("=================================")
	var count_jam int = 0

	for _, Value := range WarnetData {
		count_jam += Value.Jam
	}
	return float64(count_jam) / float64(len(WarnetData))
}

func Top3Data() []ListItems{
	var Temp []ListItems

	sort.SliceStable(WarnetData, func(i int, j int) bool {
		return WarnetData[i].Jam < WarnetData[j].Jam
	})

	for Key, Value := range WarnetData {
		Temp = append(Temp, Value)

		if Key == 2 {
			break
		}
	}

	sort.SliceStable(Temp, func(i int, j int) bool {
		return Temp[i].ID < Temp[j].ID
	})

	return Temp
}

func top() {
	fmt.Println("=================================")
	fmt.Println("       Top 3 jam terbawah        ")
	fmt.Println("=================================")
	if len(Top3Data()) > 0 {
		fmt.Println("ID\tNama\tJam\tBiaya")
		for _, Value := range Top3Data() {
			fmt.Printf("%d\t%s\t%d\t%d\n", Value.ID, Value.Nama, Value.Jam, Value.Biaya)
		}
	}else{
		fmt.Println("          Data Kosong")
	}
	fmt.Println("=================================")
	sorting()
}

func average() {
	var avg float64 = Avg()
	var check bool = false
	fmt.Println("=================================")
	fmt.Println(" Pengguna dibawah rata - rata")
	fmt.Println("=================================")
	for _, Value := range WarnetData {
		if float64(Value.Jam) < avg {
			if check == false {
				fmt.Println("ID\tNama\tJam\tBiaya")
			}
			fmt.Printf("%d\t%s\t%d\t%d\n", Value.ID, Value.Nama, Value.Jam, Value.Biaya)
			check = true
		}
	}
	if check == false {
		fmt.Println("          Data Kosong")
	}
	fmt.Println("=================================")
}
